<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $fillable = [
      'id_no',
      'tell_no',
      'adress',
      'farm_location',
      'ref_no'
    ];
}
