<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Farm;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
      // $request->validate([
      //   'id_no'=>'required',
      //   'tell_no'=>'required|unique',
      //   'adress'=>'required',
      //   'farm_location'=>'required'
      // ]);

      $create = Farm::create([
        'id_no' => $request->input('idNo'),
        'tell_no' => $request->input('tellNo'),
        'adress' => $request->input('address'),
        'farm_location' => $request->input('farmLocation'),
        'ref_no' => Str::random(10)
      ]);

      // \Log::info($create);
      if ($create){
        return view('welcome_next',compact($create))->with('success', 'Application Success');

      }

    }
}
