@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Farm Registration Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if ($message = Session::get('success'))

                      <div class="alert alert-success alert-block">

                      	<button type="button" class="close" data-dismiss="alert">×</button>

                              <strong>{{ $message }}</strong>

                      </div>

                      @endif
                <form action="{{route('farm-application')}}" method="post">
                          @csrf
                    <div class="row">

                      <div class="col-md-5">

                          <div class="form-group">
                            <label for="exampleInputEmail1">ID NO</label>
                            <input type="text" name="idNo" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter ID">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your ID with anyone else.</small>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">TELL NO</label>
                            <input type="text" name="tellNo" class="form-control" id="exampleInputPassword1" placeholder="Tell No">
                          </div>



                      </div>
                      <div class="col-md-4">


                          <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" name="address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Address">
                            <small id="emailHelp" class="form-text text-muted">Location you live</small>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Farm Location</label>
                            <input type="text" name="farmLocation" class="form-control" id="exampleInputPassword1" placeholder="Farm Location">
                          </div>

                          <button type="submit" class="btn btn-primary">Submit</button>

                      </div>

                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
