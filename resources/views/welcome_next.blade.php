@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Farm Registration Form</div>

                <div class="card-body">
                    @if (Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                   <div class="">
                     <h4>Application received, </h4>
                     <p>Tracking Number: </p>
                   </div>
                   <div class="">
                       <button type="submit" class="btn btn-primary">Download Pdf</button>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
